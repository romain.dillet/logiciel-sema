from flask import Flask, render_template, request, redirect
import requests

app = Flask(__name__)

# Chemin vers le fichier texte contenant les adresses IP des machines
MACHINE_IPS_FILE = "machine_ips.txt"

# Fonction pour lire les adresses IP à partir du fichier texte
def read_machine_ips():
    with open(MACHINE_IPS_FILE, "r") as f:
        return [line.strip() for line in f.readlines()]

# Fonction pour écrire les adresses IP dans le fichier texte
def write_machine_ips(machine_ips):
    with open(MACHINE_IPS_FILE, "w") as f:
        for ip in machine_ips:
            f.write(ip + "\n")

# Initialiser la liste des adresses IP à partir du fichier texte
MACHINE_IPS = read_machine_ips()

@app.route('/')
def home():
    machines = []
    for ip in MACHINE_IPS:
        machine = {
            'ip': ip,
            'reboot_url': f"http://{ip}:5000/reboot",
            'tableau_url': f"http://{ip}:8000/",
        }
        machines.append(machine)

    return render_template('home.html', machines=machines)

@app.route('/add_machine', methods=['POST'])
def add_machine():
    # Récupérer l'adresse IP de la nouvelle machine à partir du formulaire
    new_ip = request.form['new_ip']

    # Ajouter l'adresse IP à la liste des machines
    MACHINE_IPS.append(new_ip)

    # Écrire les adresses IP dans le fichier texte
    write_machine_ips(MACHINE_IPS)

    # Rediriger vers la page d'accueil
    return redirect('/')

@app.route('/reboot', methods=['POST'])
def reboot():
    # Récupérer l'adresse IP de la machine sélectionnée à partir du formulaire
    ip = request.form['ip']

    # Envoyer une requête de redémarrage à la machine sélectionnée
    response = requests.post(f"http://{ip}:5000/reboot")

    

    # Retourner sur la page d'accueil
    return redirect('/')

@app.route('/remove_machine', methods=['POST'])
def remove_machine():
    # Récupérer l'adresse IP de la machine à supprimer à partir du formulaire
    ip = request.form['ip']

    # Supprimer l'adresse IP de la liste des machines
    MACHINE_IPS.remove(ip)

    # Écrire les adresses IP dans le fichier texte
    write_machine_ips(MACHINE_IPS)

    # Rediriger vers la page d'accueil
    return redirect('/')


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8080)
